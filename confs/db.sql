create table if not exists `Students`(
	`id` int(16) NOT NULL auto_increment,
	`name` varchar(100) NOT NULL COLLATE utf8_persian_ci,
	`family_name` varchar(100) NOT NULL COLLATE utf8_persian_ci,
	`age` int(3) NOT NULL,
	`phone_number` varchar(11) DEFAULT NULL,
	`email` varchar(50) DEFAULT NULL,
	`password` varchar(300) NOT NULL,
	`token` varchar(200) DEFAULT NULL,
	`avatar` varchar(150) DEFAULT NULL,
	PRIMARY KEY (id),
	UNIQUE(email, phone_number, token, avatar)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

create table if not exists `Lessons`(
	`id` int(16) NOT NULL auto_increment,
	`grade` int(1) NOT NULL,
	`lesson` varchar(200) NOT NULL COLLATE utf8_persian_ci,
	PRIMARY KEY(id),
	UNIQUE(lesson)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;
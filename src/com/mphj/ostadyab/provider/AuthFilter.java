package com.mphj.ostadyab.provider;

import java.lang.reflect.Method;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import com.mphj.ostadyab.interfaces.User;
import com.mphj.ostadyab.security.Auth;
import com.mphj.ostadyab.security.SecurityContext;
import com.mphj.ostadyab.services.responses.ResponseHelper;
 
/**
 * Jersey HTTP Basic Auth filter
 * @author Deisss (LGPLv3)
 */
@Provider
public class AuthFilter implements ContainerRequestFilter {
    /**
     * Apply the filter : check input request, validate or not with user auth
     * @param containerRequest The request from Tomcat server
     */
	@Context
    private ResourceInfo resourceInfo;
    @Override
    public void filter(ContainerRequestContext containerRequest) throws WebApplicationException {
    	Method method = resourceInfo.getResourceMethod();
    	Response ACCESS_DENIED = Response.status(Response.Status.UNAUTHORIZED)
    			.type(MediaType.APPLICATION_JSON)
                .entity(ResponseHelper.JSON.UNAUTHORIZED).build();
    	Response ACCESS_FORBIDDEN = Response.status(Response.Status.FORBIDDEN)
                .entity(ResponseHelper.JSON.FORBIDDEN).build();
    	if (!method.isAnnotationPresent(PermitAll.class)){
    		if (method.isAnnotationPresent(DenyAll.class)){
    			containerRequest.abortWith(ACCESS_FORBIDDEN);
    			return;
    		}
    		if (!Auth.needAuth(method))
    			return;
    		//final MultivaluedMap<String, String> headers =containerRequest.getHeaders();
            
            //Fetch authorization header
            /*final List<String> authorization = headers.get("auth");
              
            //If no authorization information present; block access
            if(authorization == null || authorization.isEmpty())
            {
                containerRequest.abortWith(ACCESS_DENIED);
                return;
            }*/
            User user = null;
            try{
            	user = Auth.authenticate(containerRequest, method);
            }catch(SecurityException e){
        		containerRequest.abortWith(ACCESS_DENIED);
        		return;
            }
            
            SecurityContext securityContext =  new SecurityContext(user, "https");
            if (method.isAnnotationPresent(RolesAllowed.class)){
            	for (String role : ((RolesAllowed)method.getAnnotation(RolesAllowed.class)).value()){
                	if (!securityContext.isUserInRole(role)){
                		containerRequest.abortWith(ACCESS_DENIED);
                		return;
                	}
                }
            }
            containerRequest.setSecurityContext(securityContext);
    	}
        //TODO : HERE YOU SHOULD ADD PARAMETER TO REQUEST, TO REMEMBER USER ON YOUR REST SERVICE...
    }
}

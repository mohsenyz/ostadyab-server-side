package com.mphj.ostadyab;




import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;

import com.mphj.ostadyab.models.Student;
import com.mphj.ostadyab.models.Teacher;
import com.mphj.ostadyab.provider.AuthFilter;
import com.mphj.ostadyab.provider.GsonBodyHandler;
import com.mphj.ostadyab.security.Auth;

public class OstadyabApplication extends ResourceConfig{
	public static final String PACKAGE = "com.mphj.ostadyab";
	public static final String MODELS_PACKAGE = PACKAGE + ".models";
	public static final String APP_PASSWD = "eKEX25dGRWaaZMudbpbth67hCwTuUHsY4HyFpndRVdRLBaACpBUghRbwYUAJdCj7";
	
	public OstadyabApplication(){
		packages("com.mphj.ostadyab");
		register(GsonBodyHandler.class);
		register(AuthFilter.class);
		register(MultiPartFeature.class);
		Auth.register(new Student());
		Auth.register(new Teacher());
	}
	
	public static class Conf{
		public static final String DB_NAME = "testit";
		public static final String DB_USERNAME = "root";
		public static final String DB_PASS = "root";
		public static final String DB_HOST = "localhost";
		public static final String DB_PORT = "3306";
	}
}

package com.mphj.ostadyab.utils;

import org.apache.commons.validator.routines.EmailValidator;

import com.mphj.ostadyab.interfaces.ValidatorInterface;

public class Validator {
	private boolean success = true;
	public static final int EMAIL = 1, NULL = 2, NOT_NULL = 4, NOT_EMPTY = 5, NOT_NULL_AND_EMPTY = 6, NUMBER = 7;
	public static Validator newInstance(){
		return new Validator();
	}
	
	
	public Validator check(String string, int mode){
		switch (mode){
		case EMAIL:
			if (!EmailValidator.getInstance(true).isValid(string))
				success = false;
			break;
		case NULL:
			if (string != null)
				success = false;
			break;
		case NOT_NULL:
			if (string == null)
				success = false;
			break;
		case NOT_EMPTY:
			if (string.isEmpty())
				success = false;
			break;
		case NOT_NULL_AND_EMPTY:
			if (string == null || string.isEmpty())
				success = false;
			break;
		case NUMBER:
			if (string == null || string.isEmpty() || !string.matches("\\d+(\\.\\d+)?"))
				success = false;
			break;
		}
		return this;
	}
	
	public boolean isValid(){
		return success;
	}
	
	
	public Validator check(Object object, ValidatorInterface validatorInterface){
		if (!validatorInterface.isValid(object))
			success = false;
		return this;
	}
}

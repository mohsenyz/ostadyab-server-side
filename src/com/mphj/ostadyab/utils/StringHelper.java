package com.mphj.ostadyab.utils;


public class StringHelper {
	public static String join(String separator, String[] strings){
		String result = "";
		for (int i = 0; i < strings.length - 1; i++){
			result += strings[i] + separator;
		}
		result += strings[strings.length - 1];
		return result;
	}
}

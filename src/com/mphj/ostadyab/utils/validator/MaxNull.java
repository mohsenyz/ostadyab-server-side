package com.mphj.ostadyab.utils.validator;

import com.mphj.ostadyab.interfaces.ValidatorInterface;

public class MaxNull implements ValidatorInterface<String[]>{
	private int max;
	
	private MaxNull(int max){
		this.max = max;
	}
	
	public static MaxNull makeNew(int max){
		return new MaxNull(max);
	}
	
	@Override
	public boolean isValid(String[] object) {
		int nullNums = 0;
		for (String item : object){
			if (item == null)
				nullNums++;
		}
		if (nullNums <= this.max)
			return true;
		return false;
	}

}

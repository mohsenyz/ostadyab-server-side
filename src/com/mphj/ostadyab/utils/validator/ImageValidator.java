package com.mphj.ostadyab.utils.validator;

import java.io.InputStream;

import javax.imageio.ImageIO;

import com.mphj.ostadyab.interfaces.ValidatorInterface;

public class ImageValidator implements ValidatorInterface<InputStream>{

	public static ImageValidator makeNew(){
		return new ImageValidator();
	}
	
	
	@Override
	public boolean isValid(InputStream object) {
		try {
			InputStream input = object;
	        ImageIO.read(input).toString();
	        return true;
	    } catch (Exception e) {
	    }
		return false;
	}

}

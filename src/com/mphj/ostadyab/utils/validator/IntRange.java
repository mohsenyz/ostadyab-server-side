package com.mphj.ostadyab.utils.validator;

import com.mphj.ostadyab.interfaces.ValidatorInterface;

public class IntRange implements ValidatorInterface<Integer>{
	private int min;
	private int max;
	private Mode mode;
	
	public static IntRange newRange(int min, int max){
		IntRange range = new IntRange();
		range.min = min;
		range.max = max;
		range.mode = Mode.FULL_RANGE;
		return range;
	}
	
	
	public static IntRange max(int max){
		IntRange range = new IntRange();
		range.max = max;
		range.mode = Mode.MAX_ONLY;
		return range;
	}
	
	
	public static IntRange min(int min){
		IntRange range = new IntRange();
		range.min = min;
		range.mode = Mode.MIN_ONLY;
		return range;
	}
	
	
	
	@Override
	public boolean isValid(Integer object) {
		switch (this.mode){
		case MIN_ONLY:
			return object >= this.min;
		case MAX_ONLY:
			return object <= this.max;
		case FULL_RANGE:
			return (object >= this.min) && (object <= this.max);
		}
		return false;
	}
	
	
	public static enum Mode{
		MIN_ONLY, MAX_ONLY, FULL_RANGE
	}
}

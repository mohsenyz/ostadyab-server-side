package com.mphj.ostadyab.utils.validator;

import com.mphj.ostadyab.interfaces.ValidatorInterface;

public class Range implements ValidatorInterface<String>{
	
	private int min;
	private int max;
	private Mode mode;
	
	public static Range newRange(int min, int max){
		Range range = new Range();
		range.min = min;
		range.max = max;
		range.mode = Mode.FULL_RANGE;
		return range;
	}
	
	
	public static Range max(int max){
		Range range = new Range();
		range.max = max;
		range.mode = Mode.MAX_ONLY;
		return range;
	}
	
	
	public static Range min(int min){
		Range range = new Range();
		range.min = min;
		range.mode = Mode.MIN_ONLY;
		return range;
	}
	
	
	
	@Override
	public boolean isValid(String object) {
		switch (this.mode){
		case MIN_ONLY:
			return object.length() >= this.min;
		case MAX_ONLY:
			return object.length() <= this.max;
		case FULL_RANGE:
			return (object.length() >= this.min) && (object.length() <= this.max);
		}
		return false;
	}
	
	
	public static enum Mode{
		MIN_ONLY, MAX_ONLY, FULL_RANGE
	}
}

package com.mphj.ostadyab.utils;

import java.io.StringWriter;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

public class View {
	private static VelocityEngine velocityEngine;
	
	private Template template;
	private VelocityContext velocityContext;
	
	private View(Template template, VelocityContext velocityContext){
		this.template = template;
		this.velocityContext = velocityContext;
	}
	
	
	public static View from(String templ){
		Template _tempate = getEngine().getTemplate("/views/" + templ);
        VelocityContext _context = new VelocityContext();
        return new View(_tempate, _context);
	}
	
	
	public String toString(){
		StringWriter stringWriter = new StringWriter();
		template.merge(velocityContext, stringWriter);
		return stringWriter.toString();
	}
	
	
	public static void init(){
		if (!isInited()){
			velocityEngine = new VelocityEngine();
			velocityEngine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
			velocityEngine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
	        velocityEngine.init();
		}
	}
	
	
	public static VelocityEngine getEngine(){
		if (isInited())
			return velocityEngine;
		init();
		return velocityEngine;
	}
	
	
	public static boolean isInited(){
		return velocityEngine != null;
	}
}

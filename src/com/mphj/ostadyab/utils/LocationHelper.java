package com.mphj.ostadyab.utils;

import com.mphj.ostadyab.utils.location.LatLng;

/**
 * 
 * @author dahlia
 *
 */
public class LocationHelper {
	
	private static Distance defDistanceMeter = Distance.KILOMETER;
	
	public static Distance getDefaultMeter(){
		return defDistanceMeter;
	}
	
	public static void setDefaultMeter(Distance defMeter){
		defDistanceMeter = defMeter;
	}
	
	public static double distanceBetween(LatLng first, LatLng second){
		double theta = first.getLng() - second.getLng();
		double dist = Math.sin(Math.toRadians(first.getLat())) * Math.sin(Math.toRadians(second.getLat())) * Math.cos(Math.toRadians(first.getLat())) * Math.cos(Math.toRadians(second.getLat())) * Math.cos(Math.toRadians(theta));
		dist = Math.acos(dist);
		dist = Math.toDegrees(dist);
		double miles = dist * 60 * 1.1515;
		if (getDefaultMeter() == Distance.MILE){
			return miles;
		}else if (getDefaultMeter() == Distance.KILOMETER){
			return miles * 1.609344;
		}
		return miles;
	}
	
	public static enum Distance{
		KILOMETER, MILE
	}
}

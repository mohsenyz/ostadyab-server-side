package com.mphj.ostadyab.utils;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Tasks {
	private static ExecutorService excutor;
	
	static{
		excutor = Executors.newFixedThreadPool(50);
	}
	
	
	public static void inqueue(Runnable runnable){
		if (!excutor.isShutdown() && !excutor.isTerminated())
			excutor.submit(runnable);
	}
}

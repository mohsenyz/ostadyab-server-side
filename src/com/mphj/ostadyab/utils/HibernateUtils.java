package com.mphj.ostadyab.utils;

import java.sql.DriverManager;
import java.util.Properties;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

import com.mphj.ostadyab.OstadyabApplication;
import com.mphj.ostadyab.models.Lesson;

@SuppressWarnings("deprecation")
public class HibernateUtils {
	private static final SessionFactory concreteSessionFactory;
	static {
		try {
			DriverManager.registerDriver(new com.mysql.jdbc.Driver ());
			Properties prop= new Properties();
			prop.setProperty("hibernate.connection.url", "jdbc:mysql://" + 
					OstadyabApplication.Conf.DB_HOST
					+ ":" + 
					OstadyabApplication.Conf.DB_PORT + 
					"/" + 
					OstadyabApplication.Conf.DB_NAME);
			prop.setProperty("hibernate.connection.username", OstadyabApplication.Conf.DB_USERNAME);
			prop.setProperty("hibernate.connection.password", OstadyabApplication.Conf.DB_PASS);
			prop.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
			concreteSessionFactory = new AnnotationConfiguration()
					.addPackage(OstadyabApplication.MODELS_PACKAGE)
					.addAnnotatedClass(Lesson.class)
					.addProperties(prop)
					.buildSessionFactory();
		} catch (Throwable ex) {
			throw new ExceptionInInitializerError(ex);
		}
	}
	public static Session getSession()
			throws HibernateException {
		return concreteSessionFactory.openSession();
	}
}

package com.mphj.ostadyab.utils;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class DB {
	private static Session session;
	private static Transaction transaction;
	
	public static void open(){
		session = HibernateUtils.getSession();
		transaction = null;
		transaction = session.beginTransaction();
	}
	
	public static void rollback(){
		if (transaction != null)
			transaction.rollback();
	}
	
	
	public static Session getSession(){
		return session;
	}
	
	
	public static Transaction getTransaction(){
		return transaction;
	}
	
	
	public static SQLQuery getQuery(String sql){
		return session.createSQLQuery(sql);
	}
	
	
	public static void run(DBWork dbWork){
		DB.open();
		dbWork.setSession(DB.getSession());
		dbWork.run(dbWork.getSession());
		DB.close();
	}
	
	
	public static SQLQuery getQuery(String sql, Class<? extends Object> entity){
		SQLQuery sqlQuery = session.createSQLQuery(sql);
		sqlQuery.addEntity(entity);
		return sqlQuery;
	}
	
	
	public static void close(){
		if (transaction != null)
			transaction.commit();
		if (session != null)
			session.close();
	}
}

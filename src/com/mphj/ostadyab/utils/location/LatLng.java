package com.mphj.ostadyab.utils.location;


import com.mphj.ostadyab.utils.LocationHelper;

/**
 * 
 * @author dahlia
 * 
 */
public class LatLng{
	private double lat;
	private double lng;

	public LatLng(double lat, double lng) {
		this.lat = lat;
		this.lng = lng;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public String toString() {
		return lat + "mm" + lng;
	}

	public static LatLng fromString(String latLng) {
		String[] arr = latLng.split("mm");
		return new LatLng(Double.parseDouble(arr[0]), Double.parseDouble(arr[1]));
	}
	
	public double distanceTo(LatLng latLng){
		return LocationHelper.distanceBetween(this, latLng);
	}
}

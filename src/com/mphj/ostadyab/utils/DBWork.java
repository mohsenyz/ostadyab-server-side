package com.mphj.ostadyab.utils;

import org.hibernate.Session;

public abstract class DBWork{
	private Session session;

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}
	
	public abstract void run(Session session);
}

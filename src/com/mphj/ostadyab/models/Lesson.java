package com.mphj.ostadyab.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.SQLQuery;

import com.mphj.ostadyab.utils.DB;
import com.mphj.ostadyab.utils.StringHelper;

@Entity
@Table(name = "Lessons")
public class Lesson {
	private static final int GRADE_UNIVERSITY = 0,
			GRADE_HIGH_SCHOOL = 1,
			GRADE_MIDDLE_SCHOOL = 2, 
			GRADE_GRADE_SCHOOL = 3;
	
	@Id @GeneratedValue
	@Column(name = "id")
	private int id;
	
	@Column(name = "grade")
	private int grade;
	
	@Column(name = "lesson")
	private String lesson;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getGrade() {
		return grade;
	}
	public void setGrade(int grade) {
		this.grade = grade;
	}
	public String getLesson() {
		return lesson;
	}
	public void setLesson(String lesson) {
		this.lesson = lesson;
	}
	
	public static String toString(Lesson[] lessons){
		String[] lessonsId = new String[lessons.length];
		for (int i = 0; i < lessons.length; i++){
			lessonsId[i] = String.valueOf(lessons[i].id);
		}
		return StringHelper.join(",", lessonsId);
	}
	
	
	public static List<Lesson> fromString(String string){
		String[] strings = string.split(",");
		DB.open();
		SQLQuery sqlQuery = DB.getQuery("SELECT * FROM `Lessons` WHERE `id` IN (:in)", Lesson.class);
		sqlQuery.setParameter("in", StringHelper.join(",", strings));
		List<Lesson> list = sqlQuery.list();
		DB.close();
		return list;
	}
}

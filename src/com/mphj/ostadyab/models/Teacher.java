package com.mphj.ostadyab.models;

import java.lang.annotation.Annotation;

import javax.ws.rs.container.ContainerRequestContext;

import com.mphj.ostadyab.interfaces.Authenticable;
import com.mphj.ostadyab.interfaces.TeacherGuard;
import com.mphj.ostadyab.interfaces.User;
import com.mphj.ostadyab.security.Role;
import com.mphj.ostadyab.utils.location.LatLng;

public class Teacher implements User, Authenticable{
	private int id;
	private String name;
	private String familyName;
	private int age;
	private String phoneNumber;
	private String address;
	private String latLng;
	private String email;
	private String password;
	private int grade;
	private String educationPlace;
	private int priceOfMeeting;
	private long expireTime;
	
	public Teacher(){
		
	}
	
	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public LatLng getLatLng() {
		return LatLng.fromString(latLng);
	}

	public void setLatLng(LatLng latLng) {
		this.latLng = latLng.toString();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public String getEducationPlace() {
		return educationPlace;
	}

	public void setEducationPlace(String educationPlace) {
		this.educationPlace = educationPlace;
	}

	public int getPriceOfMeeting() {
		return priceOfMeeting;
	}

	public void setPriceOfMeeting(int priceOfMeeting) {
		this.priceOfMeeting = priceOfMeeting;
	}

	public long getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(long expireTime) {
		this.expireTime = expireTime;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public void setName(String name) {
		this.name = name;
	}

	private int value;
	private String avatar;
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public User authenticate(ContainerRequestContext containerRequestContext) throws SecurityException {
		return null;
	}

	@Override
	public Class<? extends Annotation> getUnique() {
		return TeacherGuard.class;
	}

	@Override
	public Role getRole() {
		return Role.makeNew(
				"teacher"
				);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}

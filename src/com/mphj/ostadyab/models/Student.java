package com.mphj.ostadyab.models;

import java.lang.annotation.Annotation;
import java.math.BigInteger;

import javax.ws.rs.container.ContainerRequestContext;

import org.hibernate.SQLQuery;

import com.mphj.ostadyab.interfaces.Authenticable;
import com.mphj.ostadyab.interfaces.StudentGuard;
import com.mphj.ostadyab.interfaces.User;
import com.mphj.ostadyab.security.Role;
import com.mphj.ostadyab.utils.DB;


public class Student implements User, Authenticable{
	private int id;
	private String name;
	private String familyName;
	private int age;
	private String phoneNumber;
	private String email;
	private String password;
	private String token;
	private String avatar;
	
	
	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public Student(String name){
		this.name = name;
	}
	
	public Student(){
		
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Override
	public Role getRole() {
		return Role.makeNew("student");
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public User authenticate(ContainerRequestContext containerRequestContext) throws SecurityException {
		return null;
	}

	@Override
	public Class<? extends Annotation> getUnique() {
		return StudentGuard.class;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
	// Helpers
	
	
	public static int countByPhone(String phone){
		SQLQuery sqlQuery = DB.getQuery("SELECT Count(*) FROM `Students` WHERE `phone_number` = :phone");
		sqlQuery.setParameter("phone", phone);
		return ((BigInteger)sqlQuery.uniqueResult()).intValue();
	}
	
	
	public static int countByEmail(String email){
		SQLQuery sqlQuery = DB.getQuery("SELECT Count(*) FROM `Students` WHERE `email` = :email");
		sqlQuery.setParameter("email", email);
		return ((BigInteger)sqlQuery.uniqueResult()).intValue();
	}
}

package com.mphj.ostadyab.security;


public class Role {
	private String[] roles;
	public static Role makeNew(String... roles){
		Role role = new Role();
		role.roles = roles;
		return role;
	}
	
	public boolean contains(String role){
		for (String rl : roles){
			System.out.println(rl + "," + role);
			return rl.equals(role);
		}
		return false;
	}
}

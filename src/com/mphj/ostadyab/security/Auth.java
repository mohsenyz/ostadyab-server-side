package com.mphj.ostadyab.security;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.container.ContainerRequestContext;

import com.mphj.ostadyab.interfaces.Authenticable;
import com.mphj.ostadyab.interfaces.User;

public class Auth {
	private static List<Authenticable> authenticableList = new ArrayList<>();
	
	public static void register(Authenticable authenticable){
		if (!exists(authenticable)){
			authenticableList.add(authenticable);
		}
	}
	
	public static User authenticate(ContainerRequestContext request, Method method)  throws SecurityException{
		for (Authenticable auth : authenticableList){
			if (method.isAnnotationPresent(auth.getUnique())){
				User user = auth.authenticate(request);
				if (user != null)
					return user;
			}
		}
		throw new SecurityException("User cannot be authenticated");
	}
	
	public static boolean exists(Authenticable authenticable){
		for (Authenticable auth : authenticableList){
			if (auth.getUnique().getName().equals(authenticable.getUnique().getName()))
				return true;
		}
		return false;
	}
	
	
	public static boolean needAuth(Method method){
		for (Authenticable auth : authenticableList){
			if (method.isAnnotationPresent(auth.getUnique())){
				return true;
			}
		}
		return false;
	}
}

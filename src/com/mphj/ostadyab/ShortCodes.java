package com.mphj.ostadyab;


import java.util.HashMap;
import java.util.Map;

import com.mphj.ostadyab.services.responses.MapMaker;

public class ShortCodes {
	private static Map<Integer, String> map;
	static{
		map = new HashMap<>();
		put(2201, "This informations are duplicate");
	}
	
	
	public static String get(int code){
		return map.get(code);
	}
	
	public static void put(int key, String value){
		map.put(key, value);
	}
}

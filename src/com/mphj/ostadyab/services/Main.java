package com.mphj.ostadyab.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.hibernate.Session;

import com.mphj.ostadyab.models.Lesson;
import com.mphj.ostadyab.services.responses.ResponseHelper;
import com.mphj.ostadyab.utils.DB;
import com.mphj.ostadyab.utils.DBWork;
import com.mphj.ostadyab.utils.Mailer;
import com.mphj.ostadyab.utils.Tasks;
import com.mphj.ostadyab.utils.Validator;
import com.mphj.ostadyab.utils.validator.Range;

@Path("/")
public class Main {
	@Context
	private ServletContext servletContext;
	
	@GET
	@Path("/hi")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Lesson> getAsHtml(@Context SecurityContext security){
		return Lesson.fromString("1");
	}
	
	
	@GET
	@Produces(MediaType.TEXT_HTML)
	public String getForm(@QueryParam("reg") String reg){
		//return View.from("helloworld.vm").toString();
		String result = "";
		Response resp = Response.status(Response.Status.UNAUTHORIZED)
		.type(MediaType.APPLICATION_JSON)
		.entity(ResponseHelper.JSON.UNAUTHORIZED).build();
		result = "" + Validator.newInstance()
				.check("ali", Range.min(3)).isValid();
		Pattern p = Pattern.compile("(\\+98|0|98)?([0-9]{10})");
		Matcher m = p.matcher(reg);
		result += m.groupCount();
		while(m.find()){
			result += "<br>" + m.group(0);
			result += "<br>" + m.group(1);
			result += "<br>" + m.group(2);
		}
		DB.run(new DBWork() {
			
			@Override
			public void run(Session session) {
				System.out.println(((BigInteger)DB.getQuery("SELECT Count(*) FROM `Lessons`").uniqueResult()).intValue());
			}
		});
		
		
		Tasks.inqueue(new Runnable() {
			
			@Override
			public void run() {
				try{
					Thread.sleep(10000);
				}catch(Exception e){
					
				}
				System.out.println("hi");
			}
		});
		return result;

	}
	
	
	@POST
	@Path("/upload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public String upload(@FormDataParam("file") InputStream fileInputStream,
			@FormDataParam("file") FormDataContentDisposition contentDispositionHeader){
		return "" + servletContext.getRealPath("images/") + "" + contentDispositionHeader.getFileName();
	}
	
	
	private void writeToFile(InputStream uploadedInputStream, String uploadedFileLocation) throws Exception {
	    OutputStream out = new FileOutputStream(new File(uploadedFileLocation));
	    int read = 0;
	    byte[] bytes = new byte[1024];

	    out = new FileOutputStream(new File(uploadedFileLocation));
	    while ((read = uploadedInputStream.read(bytes)) != -1) {
	        out.write(bytes, 0, read);
	    }
	    out.flush();
	    out.close();
	}
}

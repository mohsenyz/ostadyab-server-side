package com.mphj.ostadyab.services.responses;

import java.util.HashMap;
import java.util.Map;

public class MapMaker<KEY, VALUE>{
	private Map<KEY, VALUE> map;
	
	private MapMaker(){
		map = new HashMap<>();
	}
	
	public static <KEY, VALUE> MapMaker makeNew(){
		return new MapMaker<KEY, VALUE>();
	}
	
	
	public MapMaker puts(KEY key, VALUE value){
		map.put(key, value);
		return this;
	}
	
	public Map<KEY, VALUE> toMap(){
		return map;
	}
}

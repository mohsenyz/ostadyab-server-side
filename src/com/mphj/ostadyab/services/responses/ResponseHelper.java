package com.mphj.ostadyab.services.responses;

import java.util.HashMap;
import java.util.Map;

import com.mphj.ostadyab.interfaces.Jsonable;

public class ResponseHelper {
	public static class JSON{
		public static final SingleStatus UNAUTHORIZED = SingleStatus.makeNew(401, "You cannot access this page");
		public static final SingleStatus FORBIDDEN = SingleStatus.makeNew(403, "Access denied");
		public static final SingleStatus BAD_REQUEST = SingleStatus.makeNew(400, "Bad Request");
		public static final SingleStatus WRONG_PARAMS = SingleStatus.makeNew(400, "Wrong parameters");
	}
	
	public static class SingleStatus implements Jsonable{
		private int status;
		private String msg;
		public static SingleStatus makeNew(int status, String msg){
			return new SingleStatus(status, msg);
		}
		private SingleStatus(int status, String msg){
			this.setStatus(status);
			this.setMsg(msg);
		}
		public int getStatus() {
			return status;
		}
		public void setStatus(int status) {
			this.status = status;
		}
		public String getMsg() {
			return msg;
		}
		public void setMsg(String msg) {
			this.msg = msg;
		}
		
		public Map<String, Object> toMap(){
			Map<String, Object> map = new HashMap<>();
			map.put("status", getStatus());
			map.put("msg", getMsg());
			return map;
		}
	}
}

package com.mphj.ostadyab.services.student;


import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.RandomStringUtils;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.hibernate.Session;

import com.mphj.ostadyab.OstadyabApplication;
import com.mphj.ostadyab.ShortCodes;
import com.mphj.ostadyab.models.Student;
import com.mphj.ostadyab.services.responses.MapMaker;
import com.mphj.ostadyab.services.responses.ResponseHelper;
import com.mphj.ostadyab.utils.DB;
import com.mphj.ostadyab.utils.DBWork;
import com.mphj.ostadyab.utils.MCrypt;
import com.mphj.ostadyab.utils.Tasks;
import com.mphj.ostadyab.utils.Uploader;
import com.mphj.ostadyab.utils.Validator;
import com.mphj.ostadyab.utils.validator.ImageValidator;
import com.mphj.ostadyab.utils.validator.MaxNull;
import com.mphj.ostadyab.utils.validator.PhoneNumber;
import com.mphj.ostadyab.utils.validator.Range;

@Path("/student/registering")
public class Registering {
	@Context
	private ServletContext servletContext;
	
	
	@SuppressWarnings("unchecked")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Map<String, Object> register(
			@FormDataParam("avatar") InputStream avatar,
			@FormDataParam("avatar") FormDataContentDisposition avatarContent,
			@FormParam("name") String name,
			@FormParam("family") String family,
			@FormParam("age") String age,
			@FormParam("phone") String phoneNumber,
			@FormParam("email") String email,
			@FormParam("password") String password
			){
		// @Part Validating inputs
		boolean withEmail = false;
		
		boolean areValidInputs = Validator.newInstance()
				.check(name, Validator.NOT_NULL_AND_EMPTY)
				.check(family, Validator.NOT_NULL_AND_EMPTY)
				.check(age, Validator.NUMBER)
				.check(password, Validator.NOT_NULL_AND_EMPTY)
				.check(password, Range.newRange(8, 25))
				.check(new String[]{phoneNumber, email}, MaxNull.makeNew(1))
				.isValid();
		
		if (!areValidInputs)
			return ResponseHelper.JSON.BAD_REQUEST.toMap();
		
		if (phoneNumber != null && !phoneNumber.isEmpty() && Validator.newInstance().check(phoneNumber, PhoneNumber.newMobileNumberValidator()).isValid()){
			phoneNumber = PhoneNumber.newMobileNumberValidator().getValid(phoneNumber);
			if (phoneNumber == null)
				return ResponseHelper.JSON.BAD_REQUEST.toMap();
			withEmail = false;
		}else if (email != null && !email.isEmpty() && Validator.newInstance().check(email, Validator.EMAIL).isValid()){
			withEmail = true;
		}else {
			return ResponseHelper.JSON.BAD_REQUEST.toMap();
		}
		
		// Check is student exists
		boolean doesStudentExists = false;
		if (withEmail){
			if (Student.countByEmail(email) >= 1)
				doesStudentExists = true;
		}else{
			if (Student.countByPhone(phoneNumber) >= 1)
				doesStudentExists = true;
		}
		
		if (doesStudentExists)
			return MapMaker.makeNew()
					.puts("status", 403)
					.puts("code", 2201)
					.puts("msg", ShortCodes.get(2201))
					.toMap();
		
		// Start making new student record
		
		
		// Send register code
		sendRegistrationCode(email,  phoneNumber, withEmail);
		
		String avatarName = null;
		
		Student student = new Student();
		if (avatar != null && avatarContent.getType().contains("image")){
			if (Validator.newInstance().check(avatar, ImageValidator.makeNew()).isValid()){
				avatarName = saveStudentAvatar(avatar);
			}
		}
		String hashedPassword = MCrypt.strongHash(password);
		student.setAvatar(avatarName);
		student.setAge(Integer.valueOf(age));
		student.setEmail(email);
		student.setFamilyName(family);
		student.setName(name);
		student.setPassword(hashedPassword);
		student.setPhoneNumber(phoneNumber);
		DB.run(new DBWork() {
			@Override
			public void run(Session session) {
				session.save(student);
			}
		});
		
		return MapMaker.makeNew()
				.puts("status", 200)
				.puts("msg", "Customer have created successfully")
				.puts("ref", MCrypt.AES.encrypt("" + student.getId(), OstadyabApplication.APP_PASSWD))
				.toMap();
	}
	
	
	
	
	public static void sendRegistrationCode(String email, String phone, boolean withEmail){
		Tasks.inqueue(new Runnable() {
			
			@Override
			public void run() {
				//@TODO try sending registration code
			}
		});
	}
	
	
	public String saveStudentAvatar(final InputStream avatar){
		File dest = new File(servletContext.getRealPath("images/"));
		if (!dest.exists())
			dest.mkdir();
		String avatarName = "student_" + RandomStringUtils.randomAlphanumeric(100) + "_" + System.currentTimeMillis() + ".jpg";
		final File fullFile = new File(dest, "student_" + RandomStringUtils.randomAlphanumeric(100) + "_" + System.currentTimeMillis() + ".jpg");
		Tasks.inqueue(new Runnable() {
			
			@Override
			public void run() {
				try{
					Uploader.writeToFile(avatar, fullFile.getAbsolutePath());
				}catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		return avatarName;
	}
	
	
	
}

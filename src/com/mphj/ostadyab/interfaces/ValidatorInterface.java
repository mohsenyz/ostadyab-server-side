package com.mphj.ostadyab.interfaces;

public interface ValidatorInterface<T>{
	boolean isValid(T object);
}
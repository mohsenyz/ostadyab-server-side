package com.mphj.ostadyab.interfaces;

import java.lang.annotation.Annotation;

import javax.ws.rs.container.ContainerRequestContext;

public interface Authenticable {
	User authenticate(ContainerRequestContext containerRequestContext) throws SecurityException;
	
	Class<? extends Annotation> getUnique();
}

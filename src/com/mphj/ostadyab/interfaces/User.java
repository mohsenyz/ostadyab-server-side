package com.mphj.ostadyab.interfaces;

import java.security.Principal;

import com.mphj.ostadyab.security.Role;

public interface User extends Principal{
	public Role getRole();
}
